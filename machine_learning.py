
import cv2
import sys

import os


import numpy as np
import pickle 


face_recognizer = cv2.face.LBPHFaceRecognizer_create()


def detect_face(img):
    
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    
    
    face_cascade = cv2.CascadeClassifier('opencv-files/lbpcascade_frontalface.xml')

    
    
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.2, minNeighbors=5);
    
    
    if (len(faces) == 0):
        return None, None
    
    
    
    (x, y, w, h) = faces[0]
    
    
    return gray[y:y+w, x:x+h], faces[0]




def prepare_training_data(data_folder_path):
    
    
    
    dirs = os.listdir(data_folder_path)
    
    
    faces = []
    
    labels = []
    
    
    for dir_name in dirs:
        
        
        
        if not dir_name.startswith("s"):
            continue;
            
        
        
        
        
        label = int(dir_name.replace("s", ""))
        
        
        
        subject_dir_path = data_folder_path + "/" + dir_name
        
        
        subject_images_names = os.listdir(subject_dir_path)
        
        
        
        
        for image_name in subject_images_names:
            
            
            if image_name.startswith("."):
                continue;
            
            
            
            image_path = subject_dir_path + "/" + image_name

            
            image = cv2.imread(image_path)
            
            
            cv2.imshow("Training on image...", cv2.resize(image, (400, 500)))
            cv2.waitKey(100)
            
            
            face, rect = detect_face(image)
            
            
            
            
            if face is not None:
                
                faces.append(face)
                
                labels.append(label)
            
    cv2.destroyAllWindows()
    cv2.waitKey(1)
    cv2.destroyAllWindows()
    
    return faces, labels





def draw_rectangle(img, rect):
    (x, y, w, h) = rect
    cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)
    


def draw_text(img, text, x, y):
    cv2.putText(img, text, (x, y), cv2.FONT_HERSHEY_PLAIN, 1.5, (0, 255, 0), 2)





def predict(test_img):
    
    img = test_img.copy()
    
    face, rect = detect_face(img)

    
    label, confidence = face_recognizer.predict(face)
    
    print(confidence)
    if confidence < 50:
        label_text = subjects[label]
    else:
        label_text = subjects[0]
        label=0
    
    
    draw_rectangle(img, rect)
    
    draw_text(img, label_text, rect[0], rect[1]-5)
    
    return (img,label)

subjects=[]
def Marius(argvi,argv):
    with open("angajatii.txt","r") as f:
        global subjects
        subjects=f.readlines()

    subjects = [str(el.strip()) for el in subjects]
    subjects.insert(0,"unknown")

	
    print("Preparing data...")
    if argv == "train":
        faces, labels = prepare_training_data("training-data")
    print("Data prepared")




    print("Saving to file!")

    if argv == "train":
        with open('objs.pkl', 'wb') as f:
            pickle.dump([faces, labels], f)

    print ('Reading from file!')
	
    with open('objs.pkl', 'rb') as f:  
        faces_read, labels_read = pickle.load(f)

    faces=faces_read
    labels=labels_read
	

    print("Total faces: ", len(faces))
    print("Total labels: ", len(labels))




    face_recognizer.train(faces_read, np.array(labels_read))

    print("Predicting images...")


    test_img1 = cv2.imread(argvi)


    predicted_img1,i = predict(test_img1)
    print("Prediction complete")


    
    
    
    
    
    return subjects[i]



if __name__ == '__main__':
	print(Marius(sys.argv[1],sys.argv[2]))
